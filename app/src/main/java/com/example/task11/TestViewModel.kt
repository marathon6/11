package com.example.task11

import android.widget.EditText
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.task10.R

class TestViewModel: ViewModel(){

    var data = ""

    val currentData: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
}