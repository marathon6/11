package com.example.task11

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.task10.R

class InfoActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        val textView = findViewById<TextView>(R.id.textView)

        val surname = intent.getStringExtra("Surname")
        val name = intent.getStringExtra("Name")
        val patronymic = intent.getStringExtra("Patronymic")
        val age = intent.getStringExtra("Age")?.toInt()
        val hobby = intent.getStringExtra("Hobby")

        when(age){
            in 1..5 -> textView.text = "Добрый день, $surname $name $patronymic! \nУдивительно что в возрасте $age лет " +
                    "вы можете читать, писать и использовать данное приложение. \nИ к тому же, у вас еще есть занимательное " +
                    "хобби: $hobby. \nВ любом случае, хорошего дня!"
            in 6..17 -> textView.text = "Добрый день, $surname $name $patronymic, вам уже $age лет. \nВы скорее всего учитесь в школе, " +
                    "однако вы находите время на свое хобби: $hobby. \nСменочку на завтра уже собрали? \nПро классный час не забыли?" +
                    " \nВ любом случае, хорошего дня!"
            in 18..45 -> textView.text = "Добрый день, $surname $name $patronymic, уже $age год вы живете на этой планете, " +
                    "прошли практически все этабы жизни, попробовали многое, но ваше любимое занятие это $hobby. \n" +
                    "Занимательно, что вы находите время и на работу и на свое хобби. \n" +
                    "В любом случае, хорошего дня!"
            else -> textView.text = "Добрый день, $surname $name $patronymic, вероятно, вы уже в пенсионном возрасте, а точнее вам уже" +
                    " $age лет. \nПоявились ли у вас уже внуки? \n " +
                    "А как там ваше хобби, $hobby, оно вам всё так же интересно?\n" +
                    "В любом случае, хорошего дня!"
        }
    }
}