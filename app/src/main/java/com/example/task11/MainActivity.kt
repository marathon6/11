package com.example.task11

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.task10.R
import kotlin.random.Random
import kotlin.random.nextInt

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnGoToInfoActivity = findViewById<Button>(R.id.btnGoToInfoActivity)

        val etSurname = findViewById<EditText>(R.id.etSurname)
        val etName = findViewById<EditText>(R.id.etName)
        val etPatronymic = findViewById<EditText>(R.id.etPatronymic)
        val etAge = findViewById<EditText>(R.id.etAge)
        val etHobby = findViewById<EditText>(R.id.etHobby)

        loadData()

        btnGoToInfoActivity.setOnClickListener {
            Intent(this, InfoActivity::class.java).also {
                if (etSurname.text.toString() == "" ||
                    etName.text.toString() == "" ||
                    etPatronymic.text.toString() == "" ||
                    etAge.text.toString() == "" ||
                    etHobby.text.toString() == ""){
                        Toast.makeText(this, "Заполните все поля ввода", Toast.LENGTH_LONG).show()
                }
                else if (etAge.text.toString().toInt() in 1..120) {
                    it.putExtra("Surname", etSurname.text.toString())
                    it.putExtra("Name", etName.text.toString())
                    it.putExtra("Patronymic", etPatronymic.text.toString())
                    it.putExtra("Age", etAge.text.toString())
                    it.putExtra("Hobby", etHobby.text.toString())

                    startActivity(it)
                }
                else{
                    Toast.makeText(this, "Ваш возраст не реален, введите значение из диапазона от 1 до 120", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun saveData(){
        val etSurname = findViewById<EditText>(R.id.etSurname)
        val etName = findViewById<EditText>(R.id.etName)
        val etPatronymic = findViewById<EditText>(R.id.etPatronymic)
        val etAge = findViewById<EditText>(R.id.etAge)
        val etHobby = findViewById<EditText>(R.id.etHobby)

        val surnameText = etSurname.text.toString()
        val nameText = etName.text.toString()
        val patronymicText = etPatronymic.text.toString()
        val ageText = etAge.text.toString()
        val hobbyText = etHobby.text.toString()

        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        editor.apply{
            putString("SURNAME_KEY", surnameText)
            putString("NAME_KEY", nameText)
            putString("PATRONYMIC_KEY", patronymicText)
            putInt("AGE_KEY", ageText.toInt())
            putString("HOBBY_KEY", hobbyText)
        }.apply()

        Toast.makeText(this, "Поля ввода были сохранены", Toast.LENGTH_LONG).show()
    }

    private fun loadData(){
        val etSurname = findViewById<EditText>(R.id.etSurname)
        val etName = findViewById<EditText>(R.id.etName)
        val etPatronymic = findViewById<EditText>(R.id.etPatronymic)
        val etAge = findViewById<EditText>(R.id.etAge)
        val etHobby = findViewById<EditText>(R.id.etHobby)

        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val savedSurname = sharedPreferences.getString("SURNAME_KEY", null)
        val savedName= sharedPreferences.getString("NAME_KEY", null)
        val savedPatronymic = sharedPreferences.getString("PATRONYMIC_KEY", null)
        val savedAge = sharedPreferences.getInt("AGE_KEY", 0)
        val savedHobby = sharedPreferences.getString("HOBBY_KEY", null)

        etSurname.setText(savedSurname)
        etName.setText(savedName)
        etPatronymic.setText(savedPatronymic)
        etAge.setText(savedAge.toString())
        etHobby.setText(savedHobby)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.menuSave -> saveData()
            R.id.menuBg-> {
                val layout:ConstraintLayout = findViewById(R.id.layoutBg)
                when (Random.nextInt(1..4)){
                    1 -> layout.setBackgroundResource(R.drawable.image1)
                    2 -> layout.setBackgroundResource(R.drawable.image2)
                    3 -> layout.setBackgroundResource(R.drawable.image3)
                    4 -> layout.setBackgroundResource(R.drawable.image4)
                }
            }
            R.id.menuThirdAct->{
                Intent(this, ThirdActivity::class.java).also {
                    startActivity(it)
                }
            }
        }
        return true
    }
}