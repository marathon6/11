package com.example.task11

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.task10.R

class ThirdActivity : AppCompatActivity() {

    lateinit var viewModel: TestViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)

        val textView = findViewById<TextView>(R.id.tvThirdAct)

        viewModel = ViewModelProvider(this).get(TestViewModel::class.java)

        viewModel.currentData.observe(this, Observer {
            textView.text = it.toString()
        })



        incrementText()
    }

    private fun incrementText(){
        val editText = findViewById<EditText>(R.id.etThirdAct)
        val btn = findViewById<Button>(R.id.btnThirdAct)
        btn.setOnClickListener {
            viewModel.data = editText.text.toString()
            viewModel.currentData.value = viewModel.data
        }
    }
}